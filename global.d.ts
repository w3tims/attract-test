// this file is required to make imports from .js files
// "fixes Error when importing untyped JS modules."
// https://github.com/Microsoft/TypeScript/issues/15031#issuecomment-407131785
declare module '*';