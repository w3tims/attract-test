import { Injectable, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { Data } from 'src/assets/test-data/data';
import { City } from 'src/assets/test-data/city';
import { Category } from 'src/assets/test-data/category';

@Injectable({
  providedIn: 'root'
})
export class MockServerService implements OnInit {
  data = of(Data);
  city = of(City);
  category = of(Category);
  constructor() { }

  ngOnInit() {
    console.log(Data);
    console.log(this.data);
  }
}
