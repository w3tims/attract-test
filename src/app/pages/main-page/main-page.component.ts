import { Category } from 'src/assets/test-data/category';
import { MockServerService } from './../../services/mock-server.service';
import { Component, OnInit } from '@angular/core';

interface Book {
  id: number;
  name: string;
  city: string;
  category: string;
  price: number;
}

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  // cities = {};
  exampleBook: Book = {
    id:1,
    name:'Affiliate Marketing - A Beginner\'s Guide to Earning Online',
    city:'London',
    category:'Architecture',
    price:115
  };

  exampleBooks: Book[] = [];

  constructor(private mockServer: MockServerService) { }

  ngOnInit() {
    this.mockServer.data.subscribe((newData) => {
      console.log(newData);
    })

    for (let index = 0; index < 20; index++) {
      this.exampleBooks.push(this.exampleBook);
      
    }
  }

}
